/*
 * Copyright (C) 2022
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Yannick Ulrich
 */

#include "codepage.h"
#include "barcode.h"
#include <json-glib/json-glib.h>
#include <glib-object.h>


/**
 * PhoshCodePage:
 */
struct _PhoshCodePage {
  GtkBox                         parent;
  GtkImage                       *qrcode;
  GtkLabel                       *label;

  GCancellable                  *cancel;

};

G_DEFINE_TYPE (PhoshCodePage, phosh_codepage, GTK_TYPE_BOX);


static void
phosh_codepage_finalize (GObject *object)
{
  PhoshCodePage *self = PHOSH_CODEPAGE (object);

  g_cancellable_cancel (self->cancel);
  g_clear_object (&self->cancel);

  G_OBJECT_CLASS (phosh_codepage_parent_class)->finalize (object);
}


static void
phosh_codepage_class_init (PhoshCodePageClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->finalize = phosh_codepage_finalize;

  // g_type_ensure (PHOSH_TYPE_EVENT_LIST);

  gtk_widget_class_set_template_from_resource (widget_class,
                                               "/sm/puri/phosh/plugins/wallet/codepage.ui");
  gtk_widget_class_bind_template_child_full (widget_class,
                                             "qrcode",
                                             FALSE,
                                             G_STRUCT_OFFSET(PhoshCodePage, qrcode));
  gtk_widget_class_bind_template_child_full (widget_class,
                                             "label",
                                             FALSE,
                                             G_STRUCT_OFFSET(PhoshCodePage, label));

  gtk_widget_class_set_css_name (widget_class, "phosh-codepage");
}


void
phosh_code_page_set_code (PhoshCodePage *self, gchar *code, enum BARCODE_FORMAT type, gchar *label)
{
  GdkPixbuf *pixbuf;
  int width = 300;
  int height = 300;
  pixbuf = barcode_encode(code, type, &width, &height);
  g_warning("Actual barcode size %dx%d", width, height);
  gtk_image_set_from_pixbuf(GTK_IMAGE(self->qrcode), pixbuf);
  gtk_label_set_label (self->label, label);
}


static void
phosh_codepage_init (PhoshCodePage *self)
{
  g_autoptr (GtkCssProvider) css_provider = NULL;

  gtk_widget_init_template (GTK_WIDGET (self));

  self->cancel = g_cancellable_new ();

  css_provider = gtk_css_provider_new ();
  gtk_css_provider_load_from_resource (css_provider,
                                       "/sm/puri/phosh/plugins/wallet/stylesheet/common.css");
  gtk_style_context_add_provider (gtk_widget_get_style_context (GTK_WIDGET (self)),
                                  GTK_STYLE_PROVIDER (css_provider),
                                  GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);
}

void
phosh_code_page_load_code (PhoshCodePage *self, gchar *fn)
{
  JsonParser *parser;
  JsonNode *root;
  JsonObject *obj;
  GError *error = NULL;
  gchar *code, *label, *type_s;
  enum BARCODE_FORMAT type;

  parser = json_parser_new ();
  json_parser_load_from_file (parser, fn, &error);
  if (error)
  {
    g_print ("Unable to parse `%s': %s\n", fn, error->message);
    g_error_free (error);
    g_object_unref (parser);
    return;
  }

  root = json_parser_get_root (parser);
  obj = json_node_get_object(root);

  code = json_object_get_string_member (obj, "code");
  label= json_object_get_string_member (obj, "label");
  type_s = json_object_get_string_member (obj, "type");

  if (g_strcmp0 (type_s, "aztec") == 0)
    type = BARCODE_AZTEC;
  else if (g_strcmp0 (type_s, "code128") == 0)
    type = BARCODE_CODE128;
  else if (g_strcmp0 (type_s, "pdf417") == 0)
    type = BARCODE_PDF417;
  else if (g_strcmp0 (type_s, "qr") == 0)
    type = BARCODE_QR;
  else
  {
    g_error ("Unknown key %s", type_s);
    return;
  }

  phosh_code_page_set_code (self, code, type, label);
}

GtkWidget *
phosh_codepage_new (void)
{
  return g_object_new (PHOSH_TYPE_CODEPAGE, NULL);
}
