#ifndef PHOSH_BARCODE_H
#define PHOSH_BARCODE_H
#include <gdk-pixbuf/gdk-pixbuf.h>
enum BARCODE_FORMAT {
    BARCODE_AZTEC=0,
    BARCODE_CODE128=1,
    BARCODE_PDF417=2,
    BARCODE_QR=3
};

GdkPixbuf *barcode_encode(char *dat, enum BARCODE_FORMAT f, int *width, int *height);

#endif
