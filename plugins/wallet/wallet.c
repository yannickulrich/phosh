/*
 * Copyright (C) 2022
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Yannick Ulrich
 */

#include "codepage.h"
#include "wallet.h"
#include <handy.h>
#define WALLET_PATH "phosh-wallet"


/**
 * PhoshWallet:
 */
struct _PhoshWallet {
  GtkBox                         parent;
  GCancellable                  *cancel;
  char                          *wallet_path;
  HdyCarousel                   *carousel;

};

G_DEFINE_TYPE (PhoshWallet, phosh_wallet, GTK_TYPE_BOX);


static void
phosh_wallet_finalize (GObject *object)
{
  PhoshWallet *self = PHOSH_WALLET (object);

  g_cancellable_cancel (self->cancel);
  g_clear_object (&self->cancel);

  G_OBJECT_CLASS (phosh_wallet_parent_class)->finalize (object);
}


static void
phosh_wallet_class_init (PhoshWalletClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->finalize = phosh_wallet_finalize;

  g_type_ensure (PHOSH_TYPE_CODEPAGE);

  gtk_widget_class_set_template_from_resource (widget_class,
                                               "/sm/puri/phosh/plugins/wallet/wallet.ui");

  gtk_widget_class_bind_template_child_full (widget_class,
                                             "carousel",
                                             FALSE,
                                             G_STRUCT_OFFSET(PhoshWallet, carousel));

  gtk_widget_class_set_css_name (widget_class, "phosh-wallet");
}

static void
on_file_child_enumerated (GObject *source_object, GAsyncResult *res, gpointer user_data)
{
  g_autoptr (GError) err = NULL;
  GFile *dir = G_FILE (source_object);
  GFileEnumerator *enumerator;
  PhoshWallet *self;

  enumerator = g_file_enumerate_children_finish (dir, res, &err);
  if (enumerator == NULL) {
    g_warning ("Failed to list %s", g_file_get_basename (dir));
  }

  self = PHOSH_WALLET (user_data);

  while (TRUE) {
    GFile *file;
    GFileInfo *info;
    GtkWidget *page;
    g_autoptr (PhoshCodePage) ticket = NULL;

    if (!g_file_enumerator_iterate (enumerator, &info, &file, self->cancel, &err)) {
      g_warning ("Failed to list contents of wallet dir %s: $%s", self->wallet_path, err->message);
      return;
    }

    if (!file)
      break;

    if (g_strcmp0 (g_file_info_get_content_type (info), "application/json") != 0)
      continue;

    page = phosh_codepage_new();
    phosh_code_page_load_code (PHOSH_CODEPAGE(page), g_file_get_path(file));
    hdy_carousel_insert (self->carousel, page, 0);
  }

}


static void
load_codes (PhoshWallet *self)
{
  GFile *dir;

  self->wallet_path = g_build_filename (g_get_home_dir (), WALLET_PATH, NULL);
  dir = g_file_new_for_path (self->wallet_path);

  g_file_enumerate_children_async (dir,
                                   G_FILE_ATTRIBUTE_STANDARD_NAME ","
                                   G_FILE_ATTRIBUTE_STANDARD_SYMBOLIC_ICON ","
                                   G_FILE_ATTRIBUTE_STANDARD_DISPLAY_NAME ","
                                   G_FILE_ATTRIBUTE_TIME_MODIFIED ","
                                   G_FILE_ATTRIBUTE_STANDARD_CONTENT_TYPE,
                                   G_FILE_QUERY_INFO_NONE,
                                   G_PRIORITY_LOW,
                                   self->cancel,
                                   on_file_child_enumerated,
                                   self);
}

static void
phosh_wallet_init (PhoshWallet *self)
{
  g_autoptr (GtkCssProvider) css_provider = NULL;

  gtk_widget_init_template (GTK_WIDGET (self));

  self->cancel = g_cancellable_new ();
  load_codes (self);

  css_provider = gtk_css_provider_new ();
  gtk_css_provider_load_from_resource (css_provider,
                                       "/sm/puri/phosh/plugins/wallet/stylesheet/common.css");
  gtk_style_context_add_provider (gtk_widget_get_style_context (GTK_WIDGET (self)),
                                  GTK_STYLE_PROVIDER (css_provider),
                                  GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);
}
