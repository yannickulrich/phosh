/*
 * Copyright (C) 2022
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Yannick Ulrich
 */

#pragma once

#include <gtk/gtk.h>
#include "barcode.h"

G_BEGIN_DECLS

#define PHOSH_TYPE_CODEPAGE (phosh_codepage_get_type ())

G_DECLARE_FINAL_TYPE (PhoshCodePage, phosh_codepage, PHOSH, CODEPAGE, GtkBox)

GtkWidget *phosh_codepage_new(void);
void phosh_code_page_set_code (PhoshCodePage *self, gchar *code, enum BARCODE_FORMAT type, gchar *label);
void phosh_code_page_load_code (PhoshCodePage *self, gchar *fn);

G_END_DECLS
