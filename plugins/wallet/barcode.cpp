#include <ZXing/MultiFormatWriter.h>
#include <ZXing/BarcodeFormat.h>
#include <ZXing/TextUtfEncoding.h>
#include <ZXing/BitMatrix.h>
#include <string>
#include <gdk-pixbuf/gdk-pixbuf.h>


extern "C" {
#include "barcode.h"

GdkPixbuf *barcode_encode(char *dat, enum BARCODE_FORMAT f, int *width, int *height)
{
    ZXing::BarcodeFormat format;
    switch(f)
    {
        case BARCODE_AZTEC:
            format = ZXing::BarcodeFormat::Aztec;
            break;
        case BARCODE_CODE128:
            format = ZXing::BarcodeFormat::Code128;
            break;
        case BARCODE_PDF417:
            format = ZXing::BarcodeFormat::PDF417;
            break;
        case BARCODE_QR:
            format = ZXing::BarcodeFormat::QRCode;
            break;
        default:
            return NULL;
    }

    ZXing::MultiFormatWriter writer(format);
    std::string text, filePath;
    ZXing::BitMatrix matrix = writer.encode(ZXing::TextUtfEncoding::FromUtf8(dat), *width, *height);

    *width = matrix.width();
    *height = matrix.height();

    guint8 *buf = (guint8*) g_try_malloc_n(*height, (*width) * 3);
    if (!buf)
        return NULL;

    guint8 *it = buf;
    for (int y = 0; y < *height; ++y)
        for (int x = 0; x < *width; ++x)
            if(matrix.get(x,y))
            {
                (*it++) = 0x00;
                (*it++) = 0x00;
                (*it++) = 0x00;
            }
            else
            {
                (*it++) = 0xff;
                (*it++) = 0xff;
                (*it++) = 0xff;
            }
    return gdk_pixbuf_new_from_data(buf,
                                    GDK_COLORSPACE_RGB,
                                    false,
                                    8,
                                    *width,
                                    *height,
                                    *width*3,
                                    (GdkPixbufDestroyNotify) g_free,
                                    buf);
}

};
/*
int main()
{
    gdk_pixbuf_save(barcode_encode("Hello", BARCODE_QR), "image", "png", NULL, NULL);
}*/
