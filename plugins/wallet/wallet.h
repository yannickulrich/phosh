/*
 * Copyright (C) 2022
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Yannick Ulrich
 */


#include <gtk/gtk.h>

#pragma once

G_BEGIN_DECLS

#define PHOSH_TYPE_WALLET (phosh_wallet_get_type ())
G_DECLARE_FINAL_TYPE (PhoshWallet, phosh_wallet, PHOSH, WALLET, GtkBox)

G_END_DECLS
